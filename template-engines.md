# Template Engines

| Name | First Impression | Note |
|---|---|---|
| [Mustache](https://github.com/janl/mustache.js) | `{{#condition}}{{parameter}}{{/condition}}` | Syntax is tool simplified, sometimes have feeling of lack of clarity. Doesn't support dynamic partial |
| [EJS](https://ejs.co/) | `<% if (condition) { %><%= parameter; %><% } %>` | Doesn't have a clean look, mixing JS and template together |
| [handlebars](https://handlebarsjs.com/) | `{{#if condition}}{{parameter}}{{/if}}` | Supports dynamic partial, syntax looks clean, feature rich |
| [Nunjucks](https://mozilla.github.io/nunjucks/) | `{% if condition %}{{parameter}}{% endif %}` | Backed by Mozilla, supports dynamic partial, syntax looks clean, feature complete |
| [swig](https://node-swig.github.io/swig-templates/) | `{% if condition %}{{parameter}}{% endif %}` | Unclear if dynamic partial is supported or not. Project doesn't seem to be actively maintained |
| [Squirrelly](https://squirrelly.js.org/) | `{{@if(condition)}}{{it.parameter}}{{/if}}` | Supports dynamic partial, syntax looks clean, library size smaller than handlebars |
